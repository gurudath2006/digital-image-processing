import sys
import numpy.core.multiarray
import cv2
import glob

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):

    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image

    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv2.resize(image, dim, interpolation = inter)

    return resized
    
if __name__ == '__main__':

    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))
    
    filePaths = []
    
    if len(sys.argv) > 1:
        rootPath = sys.argv[1]
    else:
        rootPath = "C:/Users/gurud/Pictures/Samples/*.jpg"
        
    for img in glob.glob(rootPath):
        filePaths.append(img)
    
    iterator=0;
    maxCount = len(filePaths) - 1
    while(1):      
        
        imgPath = filePaths[iterator]
        cv_img = cv2.imread(imgPath)
        resize_img =  image_resize(cv_img, height = 800)
        '''resize_img = cv2.resize(cv_img, (500,500), cv2.INTER_AREA)'''
        cv2.imshow('img' , resize_img)
        '''cv2.imshow('dst_rt', cv_img)'''
        keyPressed = cv2.waitKey(0)
        print(keyPressed)
        print(cv_img.shape)
        # Browse next image using keys N or n
        if keyPressed == 78 or keyPressed == 110:
            if iterator < maxCount:
                iterator = iterator + 1
                
        #Browse next  image using keys P or p
        if keyPressed == 80 or keyPressed == 112:
            if iterator > 0:
                iterator = iterator - 1
         
        #Exit by clicking E or e
        if keyPressed == 101 or keyPressed == 69:
            cv2.destroyAllWindows()
            break;
        
    
